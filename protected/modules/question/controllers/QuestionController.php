<?php

class QuestionController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

        public function actionSaveQuestion() {
            $question = $_POST['question'] ;
            $options = $_POST['options'] ;
            $answers = $_POST['answer'] ;
            $topicID = $_POST['topic_id'] ;
            $subtopicID = $_POST['subtopic_id'] ;
            
            
            if($question == "")
                throw new CHttpException(400,'Question Blank');
            
            foreach ($options as $option) {
                if($option == "") 
                    throw new CHttpException(400,'Option Blank');
            }
            
            if(sizeof($answers) == 0) {
                throw new CHttpException(400,'No Answer Selected');
            }
            
            $questionModel = new Question ;
            
            $questionModel->topic_id = $topicID ;
            
            if($subtopicID != null && $subtopicID != "" && isset($_POST['subtopic_id']))
                $questionModel->subtopic_id = $subtopicID ;
            
            $questionModel->question_description = $question ;
            $questionModel->question_type = 1 ;
            $questionModel->created_at = new CDbExpression("NOW()") ;
            
            if($questionModel->save()) {
                $i = 1 ;
                $sols = array() ;
                foreach ($options as $option) {
                    $opt = new Multiplechoice ;
                    $opt->question_id = $questionModel->id ;
                    $opt->multiplechoice_description = $option ;
                    $opt->save() ;
                    if(in_array($i, $answers)) {
                        array_push($sols, $opt->id) ;
                    } 
                    $i++ ;                                        
                }
                
                foreach ($sols as $sol) {
                    $s = new Solution ;
                    $s->question_id = $questionModel->id ;
                    $s->multiplechoice_id = $sol ;
                    $s->save() ;
                }
                
                echo "Question Saved" ;
                
            } else {
                throw new CHttpException(400,'Could not save question, try again');
            }
        }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                $topic = Topic::model()->findByPk($_POST['topic_id']) ;
                
                $subtopic ;
                
                if(isset($_POST['subtopic_id']))
                    $subtopic = Subtopic::model ()->findByPk($_POST['subtopic_id']) ;
                
                
		$model=new Question;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Question']))
		{
			$model->attributes=$_POST['Question'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'topic'=>$topic,
			'subtopic'=>$subtopic,
		));
	}
        
        public function actionSelectSubject() {            
            
            $subjects = Subject::model()->findAll() ;
            
            $this->render('selectSubject', array(
                'subjects' => $subjects ,
            )) ;
            
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Question']))
		{
			$model->attributes=$_POST['Question'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Question');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Question('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Question']))
			$model->attributes=$_GET['Question'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Question the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Question::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Question $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='question-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
