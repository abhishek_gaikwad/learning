<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#question-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="row"> 
	<div class="col-sm-12"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Manage Questions</header> 
			<section style='overflow-x: auto'> 
			<div class="panel-body"> 
				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>
				
				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>				<br>
				<div class="col-md-6 search-form" style="display:none">
				<?php $this->renderPartial('_search',array(
					'model'=>$model,
				)); ?>
				</div><!-- search-form -->
				
			
			

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'question-grid',
				'dataProvider'=>$model->search(),
				'itemsCssClass' => 'table table-striped m-b-none text-sm',
				'filter'=>$model,
				'columns'=>array(
			 	
			array(
                    'name'=>'id',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'id', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'topic_id',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'topic_id', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'subtopic_id',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'subtopic_id', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'question_type',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'question_type', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'question_description',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'question_description', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'created_at',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'created_at', array('class'=>'input-sm form-control')),
                ),
					array(
						'class'=>'CButtonColumn',
						'htmlOptions'=>array('class'=>'col-sm-2'),
						'template'=>'<p>{view}{update}{delete}</p>',
						'deleteButtonImageUrl'=>false,
						'updateButtonImageUrl'=>false,
						'viewButtonImageUrl'=>false,
						
						'buttons'=>array
						(
							'view' => array
							(
								'label' => '<i class="fa fa-search"></i>',
								'options'=>array('title'=>'View','class'=>'btn btn-sm btn-icon btn-success', 'style'=>'margin-right: 1px;margin-left: 1px;'),
							),
							'update' => array
							(
								'label' => '<i class="fa fa-edit"></i>',
								'options'=>array('title'=>'Update','class'=>'btn btn-sm btn-icon btn-info', 'style'=>'margin-right: 1px;margin-left: 1px;'),						
							),							
							'delete' => array
							(
								'label' => '<i class="fa fa-eraser"></i>',
								'options'=>array('title'=>'Edit','class'=>'btn btn-sm btn-icon btn-danger', 'style'=>'margin-right: 1px;margin-left: 1px;'),
							),
						),
					),
				),
			)); ?>
			</div>
			</section>
			</div>
		</section>
	</div>
</div>





