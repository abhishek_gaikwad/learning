<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-9"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Select Subject</header> 
			<table class="table table-striped m-b-none text-sm">
                            <thead>
                            <th></th>
                            <th>Subject Name</th>
                            <th>Topic</th>
                            <th>Subtopic</th>
                            <th></th>
                            </thead>
                            <tbody>
                                <?php $i = 0 ; foreach($subjects as $subject) : ?>
                                <?php echo CHtml::form(CController::createUrl('create'),'POST') ?>
                                <tr>
                                    <td><?php echo ++$i ; ?></td>
                                    <td><?php echo $subject->subject_name ?></td>
                                    <td><?php echo CHtml::dropDownList('topic_id', "", CHtml::listData(Topic::model()->findAll("subject_id = $subject->id"), "id", 'topic_name') , array('class' => 'form-control input-sm topic', 'id'=> "topic_id$i", 'empty' => 'Select' )) ?></td>
                                    <td><?php echo CHtml::dropDownList('subtopic_id', "", "", array('class' => 'form-control input-sm subtopic')) ; ?></td>
                                    <td><?php echo CHtml::submitButton("Add Question", array("class" => "btn btn-sm btn-default"))?></td>
                                </tr>
                                <?php echo CHtml::endForm(); ?>
                                <?php endforeach; ?>
                                
                            </tbody>
                        </table>
                </section>
	</div>
</div>

<script>
$(window).ready(function(){
    $(".topic").change(function(){
        var val = $(this).val() ;
        var data = { 'topic_id' : val }
        var cur = $(this)
       
        if(val == 0 || val == null) {
            
        } else {
            request = $.ajax({
                        url: "<?php echo CController::createUrl('/ajaxHandler/subTopicsFromTopic'); ?>",
                        type: "post",
                        data: data,
                        
                    });
            
            request.done(function (response, textStatus, jqXHR){
               cur.parent().next().children().each(function(){
                   $(this).html(response)
               })
                
            }) ;
            
        }
        
        
    })
}); </script>
