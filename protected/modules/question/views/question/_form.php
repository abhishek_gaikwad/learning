<?php
/* @var $this QuestionController */
/* @var $model Question */
/* @var $form CActiveForm */
?>

<div class="panel-body"> 

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'question-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="text-muted">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'topic_id'); ?>
		<?php echo $form->textField($model,'topic_id', array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'topic_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'subtopic_id'); ?>
		<?php echo $form->textField($model,'subtopic_id', array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'subtopic_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'question_type'); ?>
		<?php echo $form->textField($model,'question_type',array('size'=>45,'maxlength'=>45, 'class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'question_type'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'question_description'); ?>
		<?php echo $form->textField($model,'question_description',array('size'=>45,'maxlength'=>45, 'class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'question_description'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at', array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-sm btn-default')); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->