<?php
/* @var $this QuestionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Questions',
);

?>
<div class="col-sm-12"> 
<h3 class="m-b-none m-t-sm">Questions</h3>
</div>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'summaryText'=>'', 
	'itemView'=>'_view',
)); ?>
