<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
            <?php echo CHtml::form("","POST", array('id'=>'form')) ; ?>
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Add Question</header>
			<header class="panel-heading font-bold"><?php echo $topic->subject->subject_name . " - " . $topic->topic_name . ($subtopic != null ? " - " . $subtopic->subtopic_name : "" ) ; ?></header>
                        <div class="panel-body">
                            
                            <div class="form-group" id="messageDiv"> 
                                
                            </div>
                            
                            <div class="form-group">                                
                                <?php echo CHtml::label("Enter Question") ?>
                                <?php echo CHtml::textField("question", "", array('class' => 'form-control input-sm', 'id' => 'question')) ?>
                            </div>
                            
                            <div class="form-group" id="qans">
                                <div class="input-group"> <span class="input-group-addon"> <input name="answer[]" value="1" type="checkbox"> </span> <input type="text" name="options[]" class="form-control input-sm"> </div> <br> <div class="input-group"> <span class="input-group-addon"> <input name="answer[]" value="2" type="checkbox"> </span> <input type="text" name="options[]" class="form-control input-sm"> </div> <br> <a onclick="addFields(this)" href="#">Add More</a>
                                    
                                   
                                    
                            </div>
                            
                            <div class="form-group">
                                <?php echo CHtml::submitButton("Save", array("class" => "btn btn-sm btn-default", 'id' => 'save')) ?>
                            </div>
                            
                            
                        </div>
                </section>
                <?php echo CHtml::hiddenField("topic_id", $_POST['topic_id']) ; ?>
                <?php echo CHtml::hiddenField("subtopic_id", $_POST['subtopic_id'] ) ; ?>
                
             <?php echo CHtml::endForm() ; ?>
	</div>
   
    
</div>

<script>
$(window).ready(function(){
    var $qtype ;
        
    $('#save').click(function(e){
        //alert($("form").serialize())
        
        console.log( $('#form').serialize() );
        //alert($qtype) ;
       
        
        request = $.ajax({
                        url: "<?php echo CController::createUrl('saveQuestion'); ?>",
                        type: "post",
                        data: $('#form').serialize(),                        
                    });
        request.done(function (response, textStatus, jqXHR){
            // log a message to the console
            console.log("Hooray, it worked!");
            var temp = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button> <i class="fa fa-ok-sign"></i>' + response + '</div>' ;
            $("#messageDiv").empty().append(temp) ;
            clearFields() ;
            $('#qans').html('<div class="input-group"> <span class="input-group-addon"> <input name="answer[]" value="1" type="checkbox"> </span> <input type="text" name="options[]" class="form-control input-sm"> </div> <br> <div class="input-group"> <span class="input-group-addon"> <input name="answer[]" value="2" type="checkbox"> </span> <input type="text" name="options[]" class="form-control input-sm"> </div> <br> <a onclick="addFields(this)" href="#">Add More</a>')
        });
        
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            //alert(jqXHR.responseText) ;
            var temp = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button> <i class="fa fa-ban-circle"></i> ' + jqXHR.responseText + ' </div>'
            $("#messageDiv").empty().append(temp) ;
           // clearFields() ;
            
        });
        e.preventDefault() ;
    });
})

function clearFields() {
    $('#question').val("") ;
    $('#qans').empty() ;
}

function addFields(t) {
    var val = (($("#qans").children().length - 1 ) / 2)  + 1;
    //alert(val) ;
    $(t).prev().after('<div class="input-group"> <span class="input-group-addon"> <input name="answer[]" value="' + val + '" type="checkbox"> </span> <input type="text" name="options[]" class="form-control input-sm"> </div> <br>')
}
</script>