<?php
/* @var $this QuestionController */
/* @var $data Question */
?>

<div class="col-sm-6">
<section class="panel panel-default"> 
<header class="panel-heading font-bold">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	

</header>
	<table class="table table-striped m-b-none text-sm">
	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('topic_id')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->topic_id); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('subtopic_id')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->subtopic_id); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('question_type')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->question_type); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('question_description')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->question_description); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->created_at); ?>
	</td></tr>

</table>
</section>
</div>