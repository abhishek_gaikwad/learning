<?php
//domain module
class UserController extends RController
{
        public $user ;
        
        public function init() {
            parent::init();
            $this->user = Yii::app()->session['user'] ;
        }
        
	public function actionSelect()
	{
            $domains = Domain::model()->findAll() ;
            $userDomain = $this->user->active_domain ;
            
            if(isset($_GET['id'])) {
                $d = Domain::model()->findByPk($_GET['id']) ;
                if($d == null)
                    throw new CHttpException(400, "Something went wrong") ;
                
                $this->user->setDomain($d->id) ;
                $this->redirect($this->createUrl("select")) ;
                        
            }
            
            $this->render('select', array(
                'domains' => $domains ,
                'userDomain' => $userDomain ,
            ));
	}
        
        public function actionSetUserDomain() {
            
        }
}