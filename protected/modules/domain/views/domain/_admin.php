<?php
/* @var $this DomainController */
/* @var $model Domain */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#domain-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



	<section style='overflow-x: auto'> 
			<div class="panel-body"> 
					
			
			

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'domain-grid',
				'dataProvider'=>$model->search(),
				'itemsCssClass' => 'table table-striped m-b-none text-sm',
				'filter'=>$model,
				'columns'=>array(
			
 	
			array(
                    'name'=>'domain_name',
                    'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'domain_name', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'domain_type',
                    'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'domain_type', array('class'=>'input-sm form-control')),
                ),
 	
			
					array(
						'class'=>'CButtonColumn',
						'htmlOptions'=>array('class'=>'col-sm-2'),
						'template'=>'<p>{view}{update}{delete}</p>',
						'deleteButtonImageUrl'=>false,
						'updateButtonImageUrl'=>false,
						'viewButtonImageUrl'=>false,
						
						'buttons'=>array
						(
							'view' => array
							(
								'label' => '<i class="fa fa-search"></i>',
								'options'=>array('title'=>'View','class'=>'btn btn-sm btn-icon btn-success', 'style'=>'margin-right: 1px;margin-left: 1px;'),
							),
							'update' => array
							(
								'label' => '<i class="fa fa-edit"></i>',
								'options'=>array('title'=>'Update','class'=>'btn btn-sm btn-icon btn-info', 'style'=>'margin-right: 1px;margin-left: 1px;'),						
							),							
							'delete' => array
							(
								'label' => '<i class="fa fa-eraser"></i>',
								'options'=>array('title'=>'Edit','class'=>'btn btn-sm btn-icon btn-danger', 'style'=>'margin-right: 1px;margin-left: 1px;'),
							),
						),
					),
				),
			)); ?>
			</div>
			</section>
			


