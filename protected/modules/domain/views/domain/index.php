<?php
/* @var $this DomainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Domains',
);

?>
<div class="col-sm-12"> 
<h3 class="m-b-none m-t-sm">Domains</h3>
</div>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'summaryText'=>'', 
	'itemView'=>'_view',
)); ?>
