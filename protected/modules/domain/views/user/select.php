<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-12"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Select Domain</header> 
			<div class="panel-body">
                            <?php $i = 0 ; 
                            
                             $setCol = function($i) {
                $col = array('text-primary','text-warning','text-dark','text-success','text-info','text-danger') ;
                if($i >= 6) {
                    $i = $i - 6 ;
                }
                return $col[$i] ;
            } ;
                            
                            foreach($domains as $domain) :  ?>
                            <div class="col-sm-6 col-md-3 padder-v b-r b-light <?php if($domain->id == $userDomain) echo "bg-light" ?>"> 
<span class="fa-stack fa-2x pull-left m-r-xs"> <i class="fa fa-circle fa-stack-2x <?php echo $setCol($i++)  ?>"></i> <i class="fa fa-globe fa-stack-1x text-white"></i> </span> 
<a class="clear" href="<?php echo $this->createUrl('select', array('id' => $domain->id)) ?>"> <span class="h3 block m-t"><strong><?php echo $domain->domain_name ?></strong></span></a> 

</div>  <?php if($i >= 5) $i = 0 ; ?>
                            <?php endforeach; ?>
                        </div>
                
                </section>
	</div>
	
</div>
