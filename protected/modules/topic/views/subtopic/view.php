<?php
/* @var $this SubtopicController */
/* @var $model Subtopic */

$this->breadcrumbs=array(
	'Subtopics'=>array('index'),
	$model->id,
);
?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">View Subtopic #<?php echo $model->id; ?></header>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table-striped m-b-none text-sm'),
	'attributes'=>array(
		'id',
		'subtopic_name',
		'topic_id',
	),
)); ?>

		</section>
	</div>
</div>
