<?php
/* @var $this SubtopicController */
/* @var $model Subtopic */
/* @var $form CActiveForm */
?>

<div class="panel-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control input-sm')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'subtopic_name'); ?>
		<?php echo $form->textField($model,'subtopic_name',array('size'=>45,'maxlength'=>45, 'class'=>'form-control input-sm')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'topic_id'); ?>
		<?php echo $form->textField($model,'topic_id', array('class'=>'form-control input-sm')); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton('Search', array('class'=>'btn btn-sm btn-default')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->