<?php
/* @var $this SubtopicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Subtopics',
);

?>
<div class="col-sm-12"> 
<h3 class="m-b-none m-t-sm">Subtopics</h3>
</div>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'summaryText'=>'', 
	'itemView'=>'_view',
)); ?>
