<?php
/* @var $this SubtopicController */
/* @var $data Subtopic */
?>

<div class="col-sm-6">
<section class="panel panel-default"> 
<header class="panel-heading font-bold">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	

</header>
	<table class="table table-striped m-b-none text-sm">
	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('subtopic_name')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->subtopic_name); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('topic_id')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->topic_id); ?>
	</td></tr>

</table>
</section>
</div>