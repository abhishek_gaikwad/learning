<?php
/* @var $this SubtopicController */
/* @var $model Subtopic */

$this->breadcrumbs=array(
	'Subtopics'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Create Subtopic</header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
	
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Current Subtopic</header> 
			<?php $this->renderPartial('_admin', array('model'=>$model2, 'subject_id'=>$subject_id)); ?>		</section>
	</div>
</div>
