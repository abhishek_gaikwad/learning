<?php
/* @var $this SubtopicController */
/* @var $model Subtopic */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#subtopic-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



	<section style='overflow-x: auto'> 
			<div class="panel-body"> 
				
			

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'subtopic-grid',
				'dataProvider'=>$model->search(),
				'itemsCssClass' => 'table table-striped m-b-none text-sm',
				'filter'=>$model,
				'columns'=>array(
			 	
//                        array(
//                    'value' => '$data->subject->subject_name',
//                    'name'=>'subject_id',
//                    'filter'=>CHtml::activeDropDownList($model, 'subject_id', CHtml::listData(Subject::model()->findAll(), 'id', 'subject_name') ,array('class'=>'input-sm form-control', 'empty' => 'Select')),
//                    //'filter'=>CHtml::activeTextField($model, 'subject_id', array('class'=>'input-sm form-control')),
//                ), 
                                    
                      array(
                    'value' => '$data->topic->subject->subject_name',
                    'name'=>'subject_search',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeDropDownList($model, 'subject_search', CHtml::listData(Subject::model()->findAll(), 'id', 'subject_name')  ,array('class'=>'input-sm form-control',  'empty' => 'Select')),
                ),              
                      array(
                          'value' => '$data->topic->topic_name',
                    'name'=>'topic_id',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeDropDownList($model, 'topic_id', CHtml::listData( ($subject_id == 0) ? (Topic::model()->findAll()) : (Topic::model()->findAll("subject_id = $subject_id")) , 'id', 'topic_name') ,array('class'=>'input-sm form-control', 'empty' => 'Select')),
                ), 
                        
			
			array(
                    'name'=>'subtopic_name',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'subtopic_name',  array('class'=>'input-sm form-control')),
                ),
				),
			)); ?>
			</div>
			</section>
			


