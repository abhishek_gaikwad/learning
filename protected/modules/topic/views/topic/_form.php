<?php
/* @var $this TopicController */
/* @var $model Topic */
/* @var $form CActiveForm */
?>

<div class="panel-body"> 

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'topic-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="text-muted">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <div class="form-group">
		<?php echo $form->labelEx($model,'subject_id'); ?>
		<?php echo $form->dropDownList($model, 'subject_id', CHtml::listData(Subject::model()->findAll(), 'id', 'subject_name') ,array('class'=>'input-sm form-control', 'empty' => 'Select')) ?>
		<?php echo $form->error($model,'subject_id'); ?>
	</div>
        
        
	<div class="form-group">
		<?php echo $form->labelEx($model,'topic_name'); ?>
		<?php echo $form->textField($model,'topic_name',array('size'=>45,'maxlength'=>45, 'class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'topic_name'); ?>
	</div>

	


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-sm btn-default')); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->