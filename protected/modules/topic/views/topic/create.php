<?php
/* @var $this TopicController */
/* @var $model Topic */

$this->breadcrumbs=array(
	'Topics'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Create Topic</header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
	
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Current Topic</header> 
			<?php $this->renderPartial('_admin', array('model'=>$model2)); ?>		</section>
	</div>
</div>
