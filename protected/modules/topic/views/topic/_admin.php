<?php
/* @var $this TopicController */
/* @var $model Topic */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#topic-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



	<section style='overflow-x: auto'> 
			<div class="panel-body"> 
						
			
			

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'topic-grid',
				'dataProvider'=>$model->search(),
				'itemsCssClass' => 'table table-striped m-b-none text-sm',
				'filter'=>$model,
				'columns'=>array(
			 	
			
 	
			array(
                    'name'=>'topic_name',
                    'htmlOptions'=>array('class'=>'col-sm-5'),
                    'filter'=>CHtml::activeTextField($model, 'topic_name', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'value' => '$data->subject->subject_name',
                    'name'=>'subject_id',
                    'filter'=>CHtml::activeDropDownList($model, 'subject_id', CHtml::listData(Subject::model()->findAll(), 'id', 'subject_name') ,array('class'=>'input-sm form-control', 'empty' => 'Select')),
                     ),
					
				),
			)); ?>
			</div>
			</section>
			


