<?php
/* @var $this TopicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Topics',
);

?>
<div class="col-sm-12"> 
<h3 class="m-b-none m-t-sm">Topics</h3>
</div>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'summaryText'=>'', 
	'itemView'=>'_view',
)); ?>
