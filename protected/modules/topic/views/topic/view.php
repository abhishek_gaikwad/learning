<?php
/* @var $this TopicController */
/* @var $model Topic */

$this->breadcrumbs=array(
	'Topics'=>array('index'),
	$model->id,
);
?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">View Topic #<?php echo $model->id; ?></header>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table-striped m-b-none text-sm'),
	'attributes'=>array(
		'id',
		'topic_name',
		'subject_id',
	),
)); ?>

		</section>
	</div>
</div>
