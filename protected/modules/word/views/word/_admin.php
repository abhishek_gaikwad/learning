<?php
/* @var $this WordController */
/* @var $model Word */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#word-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



	<section style='overflow-x: auto'> 
			<div class="panel-body"> 
				
							
			
			

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'word-grid',
				'dataProvider'=>$model->search(),
				'itemsCssClass' => 'table table-striped m-b-none text-sm',
				'filter'=>$model,
				'columns'=>array(
			 	
		array(
                    'name'=>'word_name',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'word_name', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'word_type',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'word_type', array('class'=>'input-sm form-control')),
                ),
 	
			
 	
			array(
                    'name'=>'word_meaning',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'word_meaning', array('class'=>'input-sm form-control')),
                ),
                                    
                       
				),
			)); ?>
			</div>
			</section>
			


