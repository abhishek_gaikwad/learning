<?php
/* @var $this WordController */
/* @var $model Word */
/* @var $form CActiveForm */
?>

<div class="panel-body"> 

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'word-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="text-muted">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'word_type'); ?>
		<?php echo $form->dropdownlist($model,'word_type',array("Adjective" => "Adjective", "Noun" => "Noun", "Verb" => "Verb", "Adverb"=> "Adverb"),array('class'=>'form-control input-sm', 'empty' => "Select")); ?>
		<?php echo $form->error($model,'word_type'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'word_name'); ?>
		<?php echo $form->textField($model,'word_name',array('size'=>45,'maxlength'=>45, 'class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'word_name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'word_meaning'); ?>
		<?php echo $form->textField($model,'word_meaning',array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'word_meaning'); ?>
	</div>
        
        <div class="form-group">
		<?php echo $form->labelEx($model,'sentence'); ?>
		<?php echo $form->textarea($model,'sentence',array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'sentence'); ?>
	</div>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-sm btn-default')); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->