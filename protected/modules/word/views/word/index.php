<?php
/* @var $this WordController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Words',
);

?>
<div class="col-sm-12"> 
<h3 class="m-b-none m-t-sm">Words</h3>
</div>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'summaryText'=>'', 
	'itemView'=>'_view',
)); ?>
