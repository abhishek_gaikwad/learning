<?php
/* @var $this WordController */
/* @var $data Word */
?>

<div class="col-sm-6">
<section class="panel panel-default"> 
<header class="panel-heading font-bold">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	

</header>
	<table class="table table-striped m-b-none text-sm">
	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('word_type')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->word_type); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('word_name')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->word_name); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('word_meaning')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->word_meaning); ?>
	</td></tr>

</table>
</section>
</div>