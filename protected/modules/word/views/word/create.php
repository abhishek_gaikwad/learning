<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Create Word</header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
	
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Current Words</header> 
			<?php $this->renderPartial('_admin', array('model'=>$model2)); ?>		</section>
	</div>
</div>
