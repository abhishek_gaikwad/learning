<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Learn Word</header> 
			<div class="panel-body">
                            
                                <div class="padder-v b-light"> 

                                    <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong><?php echo $word->word_name ?></strong></span></a>  
                                    <p class="text-muted">Type - <?php echo $word->word_type ?></p> 

                                    <p><?php echo $word->word_meaning ?></p>

                                    <strong class="">Sentence</strong>
                                    <p class="text-muted"><?php echo $word->sentence ;?></p>
                                    <br>
                                    <strong class="">Your Sentences</strong>
                                    
                                    <p class="text-muted">
                                        <?php if(sizeof($sentences) == 0) {
                                                echo "Opps, You havent added any sentences" ;
                                        }
                                            else {
                                                $i = 1 ;
                                            foreach ($sentences as $sentence) {
                                                echo $i . ") " . $sentence->sentence ;
                                               }
                                            }
                                            ?>
                                        
                                    </p>
                                        
                                    <br>
                                    <strong class="">Add Sentence</strong>
                                    <?php echo CHtml::form() ?>
                                    <?php echo CHtml::textArea('sentence',"",array('class' => 'form-control input-sm')) ?>
                                    <?php echo CHtml::submitButton("Add Sentence", array('class' => 'btn btn-default btn-sm')) ; ?>
                                    <?php echo CHtml::endForm() ?>

                                </div> 
                        </div>
                <div class="btn-group btn-group-justified"> <a href="<?php echo $this->createUrl('learn', array('id' => $word->getPreviousId()))  ?>" class="btn btn-primary">Previous</a> <a href="#" class="btn btn-info">Random</a> <a href="<?php echo $this->createUrl('learn', array('id' => $word->getNextId())) ?>" class="btn btn-success">Next</a> </div>
                </section>
	</div>
	
</div>
