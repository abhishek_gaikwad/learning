<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

function time_elapsed_string($datetime, $full = false) {
    
    $now = new DateTime;
    
    $ago = new DateTime($datetime);
    date_add($ago, date_interval_create_from_date_string('5 hours 30 minutes'));
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

?>

<div class="row"> 
	<div class="col-sm-12"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Word History</header> 
			<div class="panel-body">
                            <?php $i = 0 ; 
                            
                             $setCol = function($i) {
                $col = array('text-primary','text-warning','text-dark','text-success','text-info','text-danger') ;
                if($i >= 6) {
                    $i = $i - 6 ;
                }
                return $col[$i] ;
            } ;
                            
                            foreach($words as $word) :  ?>
                                <div class="col-sm-6 col-md-3 padder-v b-r b-light"> 
<span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x <?php echo $setCol($i++)  ?>"></i> <i class="fa fa-krw fa-stack-1x text-white"></i> </span> 
<a class="clear" href="<?php echo $this->createUrl('learn', array('id' => $word->word->id)) ?>"> <span class="h3 block m-t-xs"><strong><?php echo $word->word->word_name ?></strong></span> <small class="text-muted"><?php echo $word->count . " times - " . time_elapsed_string($word->last_seen) ?></small> </a> 

</div>  <?php if($i >= 5) $i = 0 ; ?>
                            <?php endforeach; ?>
                        </div>
                        
                
                </section>
	</div>
	
</div>
