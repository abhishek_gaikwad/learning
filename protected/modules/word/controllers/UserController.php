<?php
//word module
class UserController extends RController
{
        public $user ;
        public function init() {
            parent::init();
            $this->user = Yii::app()->session['user'] ;
        }
	public function actionAll()
	{
            $words = Word::model()->findAll(array('order'=>'word_name')) ;
            
            $this->render('all', array(
                "words" => $words ,
            )) ;
	}
        
        public function actionHistory()
	{
            $criteria = new CDbCriteria ;
            $criteria->addInCondition('user_id', array( $this->user->id)) ;
            $criteria->order = 'last_seen';
            $words = WordTracking::model()->findAll($criteria) ;
            //var_dump($words[0]->last_seen) ;
            $this->render('history', array(
                "words" => $words ,
            )) ;
	}
        
        public function actionLearn($id = 1)
	{
            $word = $this->loadModel($id) ;            
            
            $word->updateWordTracking($this->user->id) ;
            
            if(isset($_POST['sentence'])) {
                $word->addSentence($this->user->id, $_POST['sentence']) ;
            } 
            
            $this->render('learn', array(
                "word" => $word ,
                'sentences' => $word->userSentences($this->user->id),
            )) ;
	}
        
        public function loadModel($id)
	{
		$model=Word::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function actionSentences()
	{
            
            $this->render('sentences', array(
                "sentences" => $this->user->userSentences() ,
            )) ;
	}
}