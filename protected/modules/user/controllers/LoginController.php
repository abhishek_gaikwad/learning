<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
                
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();
                                        $id = Yii::app()->user->Id ;
                                        $roles=Rights::getAssignedRoles(Yii::app()->user->Id); // check for single role
                                        $session=new CHttpSession;
                                        $session->open() ;
					foreach($roles as $role) {
						
						if(Yii::app()->user->checkAccess('admin')) {
                                                        Yii::app()->session['admin'] = Userdetails::model()->find('uid =:uid', array(':uid' => Yii::app()->user->Id ));
							if (Yii::app()->user->returnUrl=='/index.php')
								$this->redirect(Yii::app()->controller->module->returnUrl);
							else
								$this->redirect(Yii::app()->user->returnUrl);
						}
                                                else if ($role->name == 'Users') {
                                                         Yii::app()->session['user'] = Userdetails::model()->find('uid =:uid', array(':uid' => Yii::app()->user->Id ));
							if (Yii::app()->user->returnUrl=='/index.php')
								$this->redirect(Yii::app()->controller->module->returnUrl);
							else
								$this->redirect(Yii::app()->user->returnUrl);
						}
						else {
							echo "<h1>no access</h1>"; //$this->redirect(array('/mailbox'));
						}
                                                
					}                            
                                        Yii::app()->user->logout();
					echo "<h1>no access</h1>"; /*
                                        if ('/index.php')
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl); */
				}
			}
			// display the login form
			$this->renderPartial('/user/login',array('model'=>$model,));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}

}