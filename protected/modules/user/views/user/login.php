
<html lang="en" class="bg-dark">
<head>
<meta charset="utf-8"/>
      <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<title>School</title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<link rel="stylesheet" href="css/app.v2.css" type="text/css"/>


<!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>
<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">
<div class="container aside-xxl">
	<a class="navbar-brand block" href="index.html">Social Learning</a>
	<section class="panel panel-default bg-white m-t-lg">
	<header class="panel-heading text-center"><strong><?php echo UserModule::t("Login"); ?></strong></header>
	<?php echo CHtml::beginForm('','post',array('class'=> 'panel-body wrapper-lg', 'data-validate' => 'parsley' )); ?>
        
        
		<div class="form-group">
                    <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>
                    <?php echo CHtml::errorSummary($model); ?>
                    </div>
		<div class="form-group">
			<?php echo CHtml::activeLabelEx($model,'username',array('class'=> 'control-label')); ?>
			<?php echo CHtml::activeTextField($model,'username',array('class'=> 'form-control input-lg', 'placeholder'=>'test@example.com', 'autocomplete'=>'off', 'data-required'=>'true')) ?>

		</div>
		<div class="form-group">
					<?php echo CHtml::activeLabelEx($model,'password',array('class'=> 'control-label')); ?>
                        <?php echo CHtml::activePasswordField($model,'password',array('class'=> 'form-control input-lg', 'placeholder'=>'password', 'autocomplete'=>'off', 'data-required'=>'true')) ?>
			
		</div>
		<div class="checkbox">
				<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
				<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
		</div>
		<?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl,array('class' =>'pull-right m-t-xs')); ?>
		
		<?php echo CHtml::submitButton(UserModule::t("Login"), array('class'=> 'btn btn-primary')); ?>

		<div class="line line-dashed">

		</div>
                    <?php echo CHtml::link("Create Account",$this->createUrl('/account/user/create'),array('class' =>'pull-right m-t-xs')); ?>
	<?php echo CHtml::endForm(); ?>
	
	</section>
	</div>
	</section>
	<!-- footer -->
	<footer id="footer">
	<div class="text-center padder">
		<p>
			<small>Learning App<br>
			&copy; 2013</small>
		</p>
	</div>
	</footer>
	<!-- / footer -->
	<script src="js/app.v2.js"></script>
	<script src="js/parsley/parsley.min.js"></script>
	<script src="js/parsley/parsley.extend.js"></script>

	<!-- Bootstrap -->
	<!-- App -->
	</body>
	</html>