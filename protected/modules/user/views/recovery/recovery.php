
<html lang="en" class="bg-dark">
<head>
<meta charset="utf-8"/>
      <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<title>School</title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<link rel="stylesheet" href="css/app.v2.css" type="text/css"/>


<!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>
<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">
<div class="container aside-xxl">
	<a class="navbar-brand block" href="index.html">School</a>
	<section class="panel panel-default bg-white m-t-lg">
	<header class="panel-heading text-center"><strong><?php echo UserModule::t("Restore"); ?></strong></header>
        <?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
        <div class="success">
        <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
        </div>
        <?php else: ?>
        
	<?php echo CHtml::beginForm('','post',array('class'=> 'panel-body wrapper-lg', 'data-validate' => 'parsley' )); ?>
        
        
		<div class="form-group">
                        
                    	<?php echo CHtml::errorSummary($form); ?>
                    </div>
		<div class="form-group">
			<?php echo CHtml::activeLabel($form,'login_or_email',array('class'=> 'control-label')); ?>
			<?php echo CHtml::activeTextField($form,'login_or_email',array('class'=> 'form-control input-lg', 'placeholder'=>'test@example.com', 'autocomplete'=>'off', 'data-required'=>'true')) ?>
            <p class="hint"><?php echo UserModule::t("Please enter your login or email addres."); ?></p>
		</div>
		
		
		<?php echo CHtml::submitButton(UserModule::t("Restore"), array('class'=> 'btn btn-primary')); ?>

		<div class="line line-dashed">
		</div>
	
	<?php echo CHtml::endForm(); ?>
	<?php endif; ?>
	</section>
	</div>
	</section>
	<!-- footer -->
	<footer id="footer">
	<div class="text-center padder">
		<p>
			<small>School App<br>
			&copy; 2013</small>
		</p>
	</div>
	</footer>
	<!-- / footer -->
	<script src="js/app.v2.js"></script>
	<script src="js/parsley/parsley.min.js"></script>
	<script src="js/parsley/parsley.extend.js"></script>

	<!-- Bootstrap -->
	<!-- App -->
	</body>
	</html>