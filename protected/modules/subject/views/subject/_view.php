<?php
/* @var $this SubjectController */
/* @var $data Subject */
?>

<div class="col-sm-6">
<section class="panel panel-default"> 
<header class="panel-heading font-bold">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	

</header>
	<table class="table table-striped m-b-none text-sm">
	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('subject_name')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->subject_name); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->created_at); ?>
	</td></tr>

</table>
</section>
</div>