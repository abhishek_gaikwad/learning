<?php
/* @var $this SubjectController */
/* @var $model Subject */

$this->breadcrumbs=array(
	'Subjects'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Update Subject <?php echo $model->id; ?></header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
</div>


