<?php
/* @var $this SubjectController */
/* @var $model Subject */

$this->breadcrumbs=array(
	'Subjects'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Create Subject</header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
	
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Current Subject</header> 
			<?php $this->renderPartial('_admin', array('model'=>$model2)); ?>		</section>
	</div>
</div>
