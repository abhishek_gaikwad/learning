<?php
/* @var $this SubjectController */
/* @var $model Subject */
/* @var $form CActiveForm */
?>

<div class="panel-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'subject_name'); ?>
		<?php echo $form->textField($model,'subject_name',array('size'=>45,'maxlength'=>45, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at', array('class'=>'form-control')); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton('Search', array('class'=>'btn btn-sm btn-default')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->