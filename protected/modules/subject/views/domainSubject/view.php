<?php
/* @var $this DomainSubjectController */
/* @var $model DomainSubject */

$this->breadcrumbs=array(
	'Domain Subjects'=>array('index'),
	$model->id,
);
?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">View DomainSubject #<?php echo $model->id; ?></header>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table-striped m-b-none text-sm'),
	'attributes'=>array(
		'id',
		'domain_id',
		'subject_id',
	),
)); ?>

		</section>
	</div>
</div>
