<?php
/* @var $this DomainSubjectController */
/* @var $model DomainSubject */
/* @var $form CActiveForm */
?>

<div class="panel-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control input-sm')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'domain_id'); ?>
		<?php echo $form->textField($model,'domain_id', array('class'=>'form-control input-sm')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'subject_id'); ?>
		<?php echo $form->textField($model,'subject_id', array('class'=>'form-control input-sm')); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton('Search', array('class'=>'btn btn-sm btn-default')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->