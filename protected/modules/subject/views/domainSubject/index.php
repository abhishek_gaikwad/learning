<?php
/* @var $this DomainSubjectController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Domain Subjects',
);

?>
<div class="col-sm-12"> 
<h3 class="m-b-none m-t-sm">Domain Subjects</h3>
</div>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'summaryText'=>'', 
	'itemView'=>'_view',
)); ?>
