<?php
/* @var $this DomainSubjectController */
/* @var $model DomainSubject */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#domain-subject-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



	<section style='overflow-x: auto'> 
			<div class="panel-body"> 
						
			
			

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'domain-subject-grid',
				'dataProvider'=>$model->search(),
				'itemsCssClass' => 'table table-striped m-b-none text-sm',
				'filter'=>$model,
				'columns'=>array(
			 	
			
 	
			array(
                    'value' => '$data->domain->domain_name',
                    'name'=>'domain_id',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeDropDownList($model, 'domain_id', CHtml::listData(Domain::model()->findAll(), 'id', 'domain_name') ,array('class'=>'input-sm form-control', 'empty' => 'Select')),
                ),
 	
			array(
                    'value' => '$data->subject->subject_name',
                    'name'=>'subject_id',
                    'filter'=>CHtml::activeDropDownList($model, 'subject_id', CHtml::listData(Subject::model()->findAll(), 'id', 'subject_name') ,array('class'=>'input-sm form-control', 'empty' => 'Select')),
                    //'filter'=>CHtml::activeTextField($model, 'subject_id', array('class'=>'input-sm form-control')),
                ),
				
				),
			)); ?>
			</div>
			</section>
			


