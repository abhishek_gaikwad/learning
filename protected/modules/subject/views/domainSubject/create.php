<?php
/* @var $this DomainSubjectController */
/* @var $model DomainSubject */

$this->breadcrumbs=array(
	'Domain Subjects'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Create DomainSubject</header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
	
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Current DomainSubject</header> 
			<?php $this->renderPartial('_admin', array('model'=>$model2)); ?>		</section>
	</div>
</div>
