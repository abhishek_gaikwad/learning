
<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-12"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Completed Test</header> 
			<div class="panel-body">
                            
                                <div class="padder-v b-light"> 
                                    
                                    <table class="table table-striped text-sm">
                                        <thead>
                                            <tr>
                                            <th>Test Name</th>   
                                            <th>Subject</th>   
                                            <th>Topic</th>   
                                            <th>Subtopic</th>   
                                            <th>Started On</th>   
                                            <th>Completed On</th>   
                                            <th>Marks</th>   
                                            <th></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php foreach ($tests as $test) : ?>
                                            <tr>
                                                <td><?php echo $test->test->test_name ?></td>
                                                <td><?php echo $test->test->subject->subject_name ?></td>
                                                <td><?php echo $test->test->topic->topic_name ?></td>
                                                <td><?php echo $test->test->subtopic->subtopic_name ?></td>
                                                <td><?php echo $test->test_started_on ?></td>
                                                <td><?php echo $test->test_completed_on ?></td>
                                                <td><?php echo $test->marks ?></td>
                                                <td><?php echo CHtml::link("View", $this->createUrl('view', array('id' => $test->id)), array('class' => 'btn btn-success btn-sm')) ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                            
                                        </tbody>
                                    </table>
                                    
                                  
                                </div> 
                        </div>
               
                </section>
	</div>
	
</div>
