
<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Test</header> 
			<div class="panel-body">
                            
                                <div class="padder-v b-light"> 
                                   <?php echo CHtml::form($this->createUrl('save', array('id'=>$session->id)), "POST") ?>     
                                   <?php foreach($session->test->testquestions as $t): ?>
                                    <div class="padder-v b-r b-light"> 
                                        <strong><?php echo $t->question->question_description ; ?></strong>
                                        
                                        <?php foreach($t->question->multiplechoices as $m) : ?>
                                        <br><?php echo CHtml::radioButton("q".$t->question->id, "", array('id' => "q".$t->question->id."m".$m->id, 'value' => $m->id )) ?> <?php echo $m->multiplechoice_description ; ?>
                                        <?php endforeach; ?>
                                    </div><br>
                                    
                                   <?php endforeach; ?>
                                    
                                   <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-success btn-sm')) ?>
                                   <?php echo CHtml::endForm() ?>
                                </div> 
                        </div>
               
                </section>
	</div>
	
</div>
