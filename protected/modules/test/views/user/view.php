
<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Test</header> 
                        <header class="panel-heading font-bold">Marks: <?php echo $session->marks ?> || Completed On : <?php echo $session->test_completed_on ?></header> 
			<div class="panel-body">
                            
                                <div class="b-light"> 
                                   <?php echo CHtml::form($this->createUrl('save', array('id'=>$session->id)), "POST") ?>     
                                   <?php $testanswer = $session->testanswers ?>
                                   <?php foreach($session->test->testquestions as $t): ?>
                                    <div class="padder-v b-r b-light"> 
                                        
                                        <strong><?php echo $t->question->question_description ; ?></strong>
                                        
                                        <?php $sol = $t->question->answer() ?>
                                        <?php $tas = Testanswer::model()->findByAttributes(array('testsession_id' => $session->id, 'question_id' => $t->question_id)) ?>
                                        
                                        <?php  foreach($t->question->multiplechoices as $m) : ?>
                                        
                                        <br><span class="<?php echo $sol->multiplechoice_id == $m->id ? "bg-success" : "" ?>"><?php echo CHtml::radioButton("", ($tas == null ? "" : ($tas->multiplechoice_id == $m->id ? true : false) ), array('id' => "q".$t->question->id."m".$m->id, 'value' => $m->id, 'disabled' => 'disabled' )) ?> <?php echo $m->multiplechoice_description ; ?></span>
                                        
                                        <?php endforeach; ?>
                                        
                                    </div><br>
                                    
                                   <?php endforeach; ?>
                                    
                                   <?php echo CHtml::endForm() ?>
                                </div> 
                        </div>
               
                </section>
	</div>
	
</div>
