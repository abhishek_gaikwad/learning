<?php
//test
class UserController extends RController
{
        public $user ;
        public function init() {
            parent::init();
            $this->user = Yii::app()->session['user'] ;
        }
        
        public function actionHistory()
	{
                $test = $this->user->completedTest();
            
            if(sizeof($test) == 0) {
                throw new CHttpException(400, "You have no completed test") ;
            }
            
            $this->render('history', array('tests' => $test));
		
	}

	public function actionPending()
	{
            $test = $this->user->pendingTest();
            
            if(sizeof($test) == 0) {
                throw new CHttpException(400, "You have no pending test") ;
            }
            
            $this->render('pending', array('tests' => $test));
	}
        
        
        public function actionSave($id) {
            $session = Testsession::model()->findByPk($id) ;
            
            if($session == null)
                throw new CHttpException(400, "No TestSession");
            
            $n = array() ;
            
            foreach($_POST as $k => $v) {
                $temp = array() ;
                array_push($temp, (int)preg_replace("/[^0-9,.]/", "", $k)) ;
                array_push($temp, (int)$v) ;
                array_push($n, $temp) ;
            } 
            array_pop($n) ;
            $session->evalute($n) ;
            $this->redirect($this->createUrl('view', array('id' => $session->id))) ;
            
        }
        
        public function actionView($id) {
             $session = Testsession::model()->findByPk($id) ;
             
             $this->render('view', array(
                    'session' => $session ,
             ));
        }


        public function actionStart($id)
	{
		$session = Testsession::model()->findByPk($id) ;
                //var_dump($session) ;
                if($session->test_completed == 1)
                    throw new CHttpException("400", "Test Completed") ;
                
                $this->render('start', array(
                    'session' => $session ,
                ));
	}

	public function actionTake($id)
	{
		$session = Testsession::takeTest((int)$this->user->id, $id) ; //var_dump($session) ;
                $this->redirect($this->createUrl('start', array('id' => $session->id))) ;
                
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}