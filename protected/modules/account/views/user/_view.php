<?php
/* @var $this UserController */
/* @var $data Userdetails */
?>

<div class="col-sm-6">
<section class="panel panel-default"> 
<header class="panel-heading font-bold">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	

</header>
	<table class="table table-striped m-b-none text-sm">
	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->first_name); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('middle_name')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->middle_name); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->last_name); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('email_id')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->email_id); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('photo_name')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->photo_name); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('photo_location')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->photo_location); ?>
	</td></tr>

	<?php /*
	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->created_at); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->uid); ?>
	</td></tr>

	*/ ?>
</table>
</section>
</div>