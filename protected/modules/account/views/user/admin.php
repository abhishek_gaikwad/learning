<?php
/* @var $this UserController */
/* @var $model Userdetails */

$this->breadcrumbs=array(
	'Userdetails'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#userdetails-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="row"> 
	<div class="col-sm-12"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Manage Userdetails</header> 
			<section style='overflow-x: auto'> 
			<div class="panel-body"> 
				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>
				
				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>				<br>
				<div class="col-md-6 search-form" style="display:none">
				<?php $this->renderPartial('_search',array(
					'model'=>$model,
				)); ?>
				</div><!-- search-form -->
				
			
			

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'userdetails-grid',
				'dataProvider'=>$model->search(),
				'itemsCssClass' => 'table table-striped m-b-none text-sm',
				'filter'=>$model,
				'columns'=>array(
			 	
			array(
                    'name'=>'id',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'id', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'first_name',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'first_name', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'middle_name',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'middle_name', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'last_name',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'last_name', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'email_id',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'email_id', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'photo_name',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'photo_name', array('class'=>'input-sm form-control')),
                ),
		/*
 	
			array(
                    'name'=>'photo_location',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'photo_location', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'created_at',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'created_at', array('class'=>'input-sm form-control')),
                ),
 	
			array(
                    'name'=>'uid',
                    //'htmlOptions'=>array('class'=>'col-sm-2'),
                    'filter'=>CHtml::activeTextField($model, 'uid', array('class'=>'input-sm form-control')),
                ),
		*/
					array(
						'class'=>'CButtonColumn',
						'htmlOptions'=>array('class'=>'col-sm-2'),
						'template'=>'<p>{view}{update}{delete}</p>',
						'deleteButtonImageUrl'=>false,
						'updateButtonImageUrl'=>false,
						'viewButtonImageUrl'=>false,
						
						'buttons'=>array
						(
							'view' => array
							(
								'label' => '<i class="fa fa-search"></i>',
								'options'=>array('title'=>'View','class'=>'btn btn-sm btn-icon btn-success', 'style'=>'margin-right: 1px;margin-left: 1px;'),
							),
							'update' => array
							(
								'label' => '<i class="fa fa-edit"></i>',
								'options'=>array('title'=>'Update','class'=>'btn btn-sm btn-icon btn-info', 'style'=>'margin-right: 1px;margin-left: 1px;'),						
							),							
							'delete' => array
							(
								'label' => '<i class="fa fa-eraser"></i>',
								'options'=>array('title'=>'Edit','class'=>'btn btn-sm btn-icon btn-danger', 'style'=>'margin-right: 1px;margin-left: 1px;'),
							),
						),
					),
				),
			)); ?>
			</div>
			</section>
			</div>
		</section>
	</div>
</div>





