<?php
/* @var $this UserController */
/* @var $model Userdetails */

$this->breadcrumbs=array(
	'Userdetails'=>array('index'),
	$model->id,
);
?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">View Userdetails #<?php echo $model->id; ?></header>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table-striped m-b-none text-sm'),
	'attributes'=>array(
		'id',
		'first_name',
		'middle_name',
		'last_name',
		'email_id',
		'photo_name',
		'photo_location',
		'created_at',
		'uid',
	),
)); ?>

		</section>
	</div>
</div>
