<?php
/* @var $this UserController */
/* @var $model Userdetails */
/* @var $form CActiveForm */
?>

<div class="panel-body"> 

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'userdetails-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="text-muted">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->errorSummary($model2,""); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>45,'maxlength'=>45, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'middle_name'); ?>
		<?php echo $form->textField($model,'middle_name',array('size'=>45,'maxlength'=>45, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'middle_name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>45,'maxlength'=>45, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email_id'); ?>
		<?php echo $form->textField($model,'email_id',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'email_id'); ?>
	</div>
        
        <div class="form-group">
		<?php echo $form->labelEx($model2,'username'); ?>
		<?php echo $form->textField($model2,'username',array('size'=>60,'maxlength'=>100, 'autocomplete'=>'off', 'class'=>'form-control')); ?>
		<?php echo $form->error($model2,'username'); ?>
	</div>
        
        <div class="form-group">
		<?php echo $form->labelEx($model2,'password'); ?>
		<?php echo $form->passwordField($model2,'password',array('size'=>60,'maxlength'=>100, 'autocomplete'=>'off', 'class'=>'form-control')); ?>
		<?php echo $form->error($model2,'password'); ?>
	</div>
        
        <div class="form-group">
		<?php echo $form->labelEx($model2,'repeatpassword'); ?>
		<?php echo $form->passwordField($model2,'repeatpassword',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model2,'repeatpassword'); ?>
	</div>

	


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-sm btn-default')); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->