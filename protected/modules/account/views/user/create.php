
<html lang="en" class="bg-dark">
<head>
<meta charset="utf-8"/>
      <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<title>School</title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<link rel="stylesheet" href="css/app.v2.css" type="text/css"/>


<!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>
<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">
<div class="container aside-xxl">
	<a class="navbar-brand block" href="index.html">Social Learning</a>
	<section class="panel panel-default bg-white m-t-lg">
	<header class="panel-heading text-center"><strong><?php echo 'Create Account'; ?></strong></header>
            <?php $this->renderPartial('_form', array('model'=>$model,'model2'=>$model2)); ?>
	
	</section>
	</div>
	</section>
	<!-- footer -->
	<footer id="footer">
	<div class="text-center padder">
		<p>
			<small>Learning App<br>
			&copy; 2013</small>
		</p>
	</div>
	</footer>
	<!-- / footer -->
	<script src="js/app.v2.js"></script>
	<script src="js/parsley/parsley.min.js"></script>
	<script src="js/parsley/parsley.extend.js"></script>

	<!-- Bootstrap -->
	<!-- App -->
	</body>
	</html>