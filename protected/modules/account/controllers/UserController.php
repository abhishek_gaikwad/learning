<?php

class UserController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{		
            return array( 
                'rights', 
            ); 
	}
        
        public function allowedActions() 
        { 
            return 'index, create'; 
        } 

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Userdetails;
                $model2 = new User ;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Userdetails']) && isset($_POST['User']))
		{
			$model->attributes=$_POST['Userdetails'];
			$model2->attributes=$_POST['User'];
                        
                        $profile=new Profile;
                        
                        $model->created_at = time();
                        
                        
                        $model2->activkey=UserModule::encrypting(microtime().$model2->password);
                        $model2->createtime=time();
                        $model2->lastvisit=time();
                        $profile->lastname=$model->last_name;
                        $profile->firstname=$model->first_name;
                        $profile->user_id=0;
                        
                        $model2->email = $model->email_id ;
                        
                        $first = $model->validate() ;
                        $second = $model2->validate() ;
                        $third = $profile->validate() ;
                        if($first && $second && $third) {
                                                                
                                $model2->password=UserModule::encrypting($model2->password);
                                $model2->repeatpassword=UserModule::encrypting($model2->repeatpassword);
                                if($model->save() && $model2->save()) {
                                    $profile->user_id=$model2->id;
                                    $profile->save();
                                    $authorizer = Yii::app()->getModule("rights")->getAuthorizer();
                                    $authorizer->authManager->assign('Users', $model2->id); 
                                    $model->saveAttributes(array('uid'=>$model2->id));
                                    $model2->saveAttributes(array('status'=>1));
                                    $this->redirect(array('view','id'=>$model->id));
                                    
                                }
                                                            
                        }
				
		}
                
		$this->renderPartial('create',array(
			'model'=>$model,
			'model2'=>$model2,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Userdetails']))
		{
			$model->attributes=$_POST['Userdetails'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Userdetails');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Userdetails('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Userdetails']))
			$model->attributes=$_GET['Userdetails'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Userdetails the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Userdetails::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Userdetails $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='userdetails-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
