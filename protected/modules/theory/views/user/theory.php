<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-12"> 
		<section class="panel panel-default"> 
                    <header class="panel-heading font-bold"><?php echo $theory->topic->topic_name ; echo $theory->subtopic_id != null ?  " - " . $theory->subtopic->subtopic_name : "" ?></header> 
			<div class="panel-body">
                            <?php echo nl2br($theory->theory_description) ; ?>
                        </div>
                    <footer class="panel-footer">
                        <div>                            
                            <a class="btn btn-default btn-sm btn-dark" href="<?php echo $this->createUrl("/test/user/take", array('id' => $theory->id)) ?>">Take Test</a>
                        </div>
                    </footer>
                
                </section>
	</div>
	
</div>
