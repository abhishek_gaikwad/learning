<?php
/* @var $this WordController */
/* @var $model Word */

$this->breadcrumbs=array(
	'Words'=>array('index'),
	'Create',
);
Yii::app()->clientScript->registerCoreScript('nestable');   
?>

<div class="row"> 
	<div class="col-sm-12"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold"><?php echo $subject->subject_name ?></header> 
			<div class="panel-body">
                            <div class="dd" id="nestable1">
                                <ol class="dd-list">
                                <?php $i = 1 ; $j = 1 ; ?>
                                <?php  foreach($topics as $topic) : ?>
                                    <li class="dd-item" data-id="1">                                        
                                        <div class="dd-handle"><a href="<?php echo $this->createUrl('theory', array('id' => Theory::model()->find("topic_id = $topic->id AND subtopic_id IS NULL")->id)) ?>"><?php $j = 1 ; echo $i . '. ' .$topic->topic_name ; ?></a></div>
                                        <?php if(sizeof($topic->subtopics) >= 1) : ?>
                                            <ol class="dd-list">
                                            <?php foreach($topic->subtopics as $subtopic) : ?>
                                                <div class="dd-handle"><a href="<?php echo $this->createUrl('theory', array('id' => Theory::model()->find("topic_id = $topic->id AND subtopic_id = $subtopic->id")->id)) ?>"><?php echo $i . '.' . $j++ . '. ' . $subtopic->subtopic_name ; ?></a></div>   
                                            <?php endforeach; ?>
                                            </ol>
                                        <?php endif; ?>
                                    </li>
                                <?php  $i++ ;  endforeach; ?>
                                </ol>
                            </div>
                        </div>
                
                </section>
	</div>
	
</div>
