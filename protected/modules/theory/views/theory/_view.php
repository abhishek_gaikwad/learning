<?php
/* @var $this TheoryController */
/* @var $data Theory */
?>

<div class="col-sm-6">
<section class="panel panel-default"> 
<header class="panel-heading font-bold">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	

</header>
	<table class="table table-striped m-b-none text-sm">
	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('topic_id')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->topic_id); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('subtopic_id')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->subtopic_id); ?>
	</td></tr>

	<tr><td><b><?php echo CHtml::encode($data->getAttributeLabel('theory_description')); ?>:</b></td>
	<td><?php echo CHtml::encode($data->theory_description); ?>
	</td></tr>

</table>
</section>
</div>