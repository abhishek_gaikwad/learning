<?php
/* @var $this TheoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Theories',
);

?>
<div class="col-sm-12"> 
<h3 class="m-b-none m-t-sm">Theories</h3>
</div>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'summaryText'=>'', 
	'itemView'=>'_view',
)); ?>
