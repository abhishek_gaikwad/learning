<?php
/* @var $this TheoryController */
/* @var $model Theory */

$this->breadcrumbs=array(
	'Theories'=>array('index'),
	$model->id,
);
?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">View Theory #<?php echo $model->id; ?></header>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table-striped m-b-none text-sm'),
	'attributes'=>array(
		'id',
		'topic_id',
		'subtopic_id',
		'theory_description',
	),
)); ?>

		</section>
	</div>
</div>
