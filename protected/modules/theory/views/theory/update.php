<?php
/* @var $this TheoryController */
/* @var $model Theory */

$this->breadcrumbs=array(
	'Theories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Update Theory <?php echo $model->id; ?></header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
</div>


