<?php
/* @var $this TheoryController */
/* @var $model Theory */

$this->breadcrumbs=array(
	'Theories'=>array('index'),
	'Create',
);

?>

<div class="row"> 
	<div class="col-sm-6"> 
		<section class="panel panel-default"> 
			<header class="panel-heading font-bold">Create Theory</header> 
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</section>
	</div>
</div>
