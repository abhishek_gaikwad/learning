<?php
/* @var $this TheoryController */
/* @var $model Theory */
/* @var $form CActiveForm */
?>

<div class="panel-body"> 

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'theory-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="text-muted">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <div class="form-group">
            <?php echo CHtml::label('Subject'); ?>
            <?php echo  CHtml::dropDownList('subject_id', '', CHtml::listData(Subject::model()->findAll(), 'id', 'subject_name') ,array('class'=>'input-sm form-control', 'empty' => 'Select', 'ajax' => array(
'type'=>'POST',
'url'=>CController::createUrl('/ajaxHandler/topicsFromSubject'),
'update'=>'#topics',
))) ?>
	</div>
        
	<div class="form-group">
		<?php echo $form->labelEx($model,'topic_id'); ?>
		<?php echo $form->dropDownList($model,'topic_id', "",array('class'=>'form-control input-sm', 'id' => 'topics', 'ajax' => array(
'type'=>'POST',
'url'=>CController::createUrl('/ajaxHandler/subTopicsFromTopic'),
'update'=>'#subtopics',
))); ?>
		<?php echo $form->error($model,'topic_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'subtopic_id'); ?>
		<?php echo $form->dropDownList($model,'subtopic_id', "" ,array('class'=>'form-control input-sm', 'id' => 'subtopics')); ?>
		<?php echo $form->error($model,'subtopic_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'theory_description'); ?>
		<?php echo $form->textArea($model,'theory_description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'theory_description'); ?>
	</div>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-sm btn-default')); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->