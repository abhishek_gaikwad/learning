<?php
//theory
class UserController extends RController
{
    public $user ;
        
        public function init() {
            parent::init();
            $this->user = Yii::app()->session['user'] ;
        }
        
	public function actionSubject()
	{
            $domain = $this->user->activeDomain ;
            $subjects = $domain->domainSubjects ;
            $this->render('subject', array(
                'domain' => $domain,
                'subjects' => $subjects,
            ));
	}
        
        public function actionContent($id) {
            $subject = Subject::model()->findbypk($id) ;
            
            if($subject == null)
                throw new CHttpException('404', 'No Subject Found');
            
            $topics = $subject->topics ;
            $this->render('content', array(
                'subject' => $subject,
                'topics' => $topics,
            )) ;
            
        }
        
        public function actionTheory($id)
	{
                $theory = Theory::model()->findByPk($id) ;
            
            if($theory == null)
                throw new CHttpException('404', 'No Subject Found');
            
		$this->render('theory', array(
                    'theory' => $theory ,
                ));
	}
}