<?php

class AjaxHandlerController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
        protected function search_in_array($srchvalue, $array)
        {
                if (is_array($array) && count($array) > 0)
                {
                        $foundkey = array_search($srchvalue, $array);
                        if ($foundkey === FALSE)
                        {
                                foreach ($array as $key => $value)
                                {
                                        if (is_array($value) && count($value) > 0)
                                        {
                                                $foundkey = search_in_array($srchvalue, $value);
                                                if ($foundkey != FALSE)
                                                        return $foundkey;
                                        }
                                }
                        }
                        else
                                return $foundkey;
                }
        }
        
        public function searchArrayValueByKey(array $array, $search) {
            foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($array)) as $key => $value) {
                if ($search === $key)
                    return $value;
            }
            return false;
        }
        
        public function actionTopicsFromSubject() {
            $data= Topic::model()->findAll('subject_id=:subject_id', 
                          array(':subject_id'=>(int) $this->searchArrayValueByKey($_POST, 'subject_id') ));

            $data=CHtml::listData($data,'id','topic_name');
            echo CHtml::tag('option',array('value'=>''),'Select',true);
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
            
           
        }
        public function actionSubTopicsFromTopic() {
            $data= Subtopic::model()->findAll('topic_id=:topic_id', 
                          array(':topic_id'=>(int) $this->searchArrayValueByKey($_POST, 'topic_id') ));

            $data=CHtml::listData($data,'id','subtopic_name');
            echo CHtml::tag('option',array('value'=>''),'Select',true);
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
            
           
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}