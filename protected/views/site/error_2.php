<!DOCTYPE html>
<html lang="en" class="">
   <head>
      <meta charset="utf-8" />
      <title>Error</title>
      <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
      <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
      <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
      <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
   </head>
   <body>
      <section id="content">
   <div class="row m-n">
      <div class="col-sm-4 col-sm-offset-4">
         <div class="text-center m-b-lg">
            <h1 class="h text-white animated fadeInDownBig"><?php echo $code; ?></h1>
         </div>
		 <div class="m-b-sm m-b-lg text-center">
		 <?php echo CHtml::encode($message); ?>
		 </div>
         <div class="list-group m-b-sm bg-white m-b-lg"> <a href="index.php" class="list-group-item"> <i class="fa fa-chevron-right icon-muted"></i> <i class="fa fa-fw fa-home icon-muted"></i> Goto homepage </a> 
		 </div>
      </div>
   </div>
</section>
      <footer id="footer">
         <div class="text-center padder clearfix">
            <p> <small>Mobile first web app framework base on Bootstrap<br>&copy; 2013</small> </p>
         </div>
      </footer>
	        <script type="text/javascript" src="/sams/js/jquery-1.11.0.js"></script>
      <!-- / footer --> <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
   </body>
</html>







