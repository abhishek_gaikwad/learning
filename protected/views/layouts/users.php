<!DOCTYPE html>
<html lang="en" class="app">
   <head>
      <meta charset="utf-8"/>
      <title><?php echo CHtml::encode($this->pageTitle); ?></title>
      <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
      <link rel="stylesheet" href="css/app.v2.css" type="text/css"/>
      <link rel="stylesheet" href="css/font.css" type="text/css" cache="false"/>
      <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
	  <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	  <?php Yii::app()->clientScript->registerCoreScript('app'); ?>
   </head>
   <body>
      <section class="vbox">
         <header class="bg-dark dk header navbar navbar-fixed-top-xs">
            <div class="navbar-header aside-md">
               <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"><i class="fa fa-bars"></i></a><a href="#" class="navbar-brand" data-toggle="fullscreen"><img src="images/logo.png" class="m-r-sm">E-Learning</a><a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"><i class="fa fa-cog"></i></a>
            </div>
            
            <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
               
               <li class="dropdown hidden-xs">
                  <a href="#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a>
                  <section class="dropdown-menu aside-xl animated fadeInUp">
                     <section class="panel bg-white">
                        <form role="search">
                           <div class="form-group wrapper m-b-none">
                              <div class="input-group">
                                 <input type="text" class="form-control" placeholder="Search"><span class="input-group-btn"><button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button></span>
                              </div>
                           </div>
                        </form>
                     </section>
                  </section>
               </li>
               <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="thumb-sm avatar pull-left"><img src="images/avatar.jpg"></span> <?php echo Yii::app()->session['user']->first_name ; ?> <b class="caret"></b></a>
                  <ul class="dropdown-menu animated fadeInRight">
                     <span class="arrow top"></span>
                     
                     <li class="divider"></li>
                     <li><a href="<?php echo $this->createUrl('/user/logout'); ?>">Logout</a></li>
                  </ul>
               </li>
            </ul>
         </header>
         <section>
            <section class="hbox stretch">
               <!-- .aside -->
               <aside class="bg-light lter b-r aside-md hidden-print" id="nav">
                  <section class="vbox">
                     <section class="w-f scrollable">
                        <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
                           <!-- nav -->
                           <nav class="nav-primary hidden-xs">
                               
<?php $this->widget('zii.widgets.CMenu', array(
		'htmlOptions'=>array('class'=>'nav'),
                'items' => array(
                array(
                        'label'=>'<i class="fa fa-home icon"><b class="bg-success"></b></i><span>Home</span>', 
                        'url'=>array('/home')
                ),
                array(
                        'label'=>'<i class="fa fa-quote-left icon"><b class="bg-primary"></b></i><span class="pull-right"><i class="fa fa-angle-down text"></i><i class="fa fa-angle-up text-active"></i></span><span>Words</span>', 
                        'url'=>array('/employee'),
                        'active' => (isset($this->module->id) && ($this->module->id == 'word')) ? true : false ,
                        'items'=> array(
                            array(
                                        'label' => '<i class="fa fa-angle-right"></i><span>All Words</span>',
                                        'url' => array('/word/user/all'),
                            ),
//                            array(
//                                    'label' => '<i class="fa fa-angle-right"></i><span>Learn Words</span>',
//                                    'url' => array('/word/user/learn'),
//                            ),
                            array(
                                        'label' => '<i class="fa fa-angle-right"></i><span>Learned Words</span>',
                                        'url' => array('/word/user/history'),
                            ),
                            array(
                                    'label' => '<i class="fa fa-angle-right"></i><span>Your Sentences</span>',
                                    'url' => array('/word/user/sentences'),
                            ),
                                
                        ),
                ),
                array(
                        'label'=>'<i class="fa fa-edit icon"><b class="bg-danger"></b></i><span class="pull-right"><i class="fa fa-angle-down text"></i><i class="fa fa-angle-up text-active"></i></span><span>Theory</span>', 
                        'url'=>array('/student'),
                        'active' => (isset($this->module->id) && ($this->module->id == 'theory' || $this->module->id == 'domain' )) ? true : false ,
                        'items'=> array(
                                array(
                                        'label' => '<i class="fa fa-angle-right"></i><span>All Domains</span>',
                                        'url' => array('/domain/user/select'),
                                ),
                                array(
                                        'label' => '<i class="fa fa-angle-right"></i><span>Theory</span>',
                                        'url' => array('/theory/user/subject'),
                                ),
                                	
                        ),	
                ),
                array(
                        'label'=>'<i class="fa fa-question-circle icon"><b class="bg-dark"></b></i><span class="pull-right"><i class="fa fa-angle-down text"></i><i class="fa fa-angle-up text-active"></i></span><span>Test</span>', 
                        'url'=>array('/student'),
                        'active' => (isset($this->module->id) && ($this->module->id == 'test')) ? true : false ,
                        'items'=> array(
//                                array(
//                                        'label' => '<i class="fa fa-angle-right"></i><span>Take Test</span>',
//                                        'url' => array('/test/user/test'),
//                                ),
                                array(
                                        'label' => '<i class="fa fa-angle-right"></i><span>Pending Test</span>',
                                        'url' => array('/test/user/pending'),
                                ),
                                array(
                                        'label' => '<i class="fa fa-angle-right"></i><span>Test History</span>',
                                        'url' => array('/test/user/history'),
                                ),
                                	
                        ),	
                ),
                
					
                ),
                'encodeLabel' => false,                
                'submenuHtmlOptions' => array(
                    'class' => 'nav lt',
                )
            ));?>


                           </nav>
                           <!-- / nav -->
                        </div>
                     </section>
                     <footer class="footer lt hidden-xs b-t b-light">
                        <div id="chat" class="dropup">
                           <section class="dropdown-menu on aside-md m-l-n">
                              <section class="panel bg-white">
                                 <header class="panel-heading b-b b-light">Active chats</header>
                                 <div class="panel-body animated fadeInRight">
                                    <p class="text-sm">
                                       No active chats.
                                    </p>
                                    <p>
                                       <a href="#" class="btn btn-sm btn-default">Start a chat</a>
                                    </p>
                                 </div>
                              </section>
                           </section>
                        </div>
                        <div id="invite" class="dropup">
                           <section class="dropdown-menu on aside-md m-l-n">
                              <section class="panel bg-white">
                                 <header class="panel-heading b-b b-light"> Abhishek <i class="fa fa-circle text-success"></i></header>
                                 <div class="panel-body animated fadeInRight">
                                    <p class="text-sm">
                                       No contacts in your lists.
                                    </p>
                                    <p>
                                       <a href="#" class="btn btn-sm btn-facebook"><i class="fa fa-fw fa-facebook"></i> Invite from Facebook</a>
                                    </p>
                                 </div>
                              </section>
                           </section>
                        </div>
                        <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-default btn-icon"><i class="fa fa-angle-left text"></i><i class="fa fa-angle-right text-active"></i></a>
                        <div class="btn-group hidden-nav-xs">
                           <button type="button" title="Chats" class="btn btn-icon btn-sm btn-default" data-toggle="dropdown" data-target="#chat"><i class="fa fa-comment-o"></i></button><button type="button" title="Contacts" class="btn btn-icon btn-sm btn-default" data-toggle="dropdown" data-target="#invite"><i class="fa fa-facebook"></i></button>
                        </div>
                     </footer>
                  </section>
               </aside>
               <!-- /.aside -->
			   
			   
               <section id="content">
					<div class="vbox">
					
					<section class="scrollable padder">
						<?php if(isset($this->breadcrumbs)):
 
						if ( Yii::app()->controller->route !== 'site/index' )
							$this->breadcrumbs = array_merge(array (Yii::t('zii','Home')=>Yii::app()->homeUrl), $this->breadcrumbs);
					 
						$this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
							'homeLink'=>false,
							'tagName'=>'ul',
							'separator'=>'',
							'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
							'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
							'htmlOptions'=>array ('class'=>'breadcrumb no-border no-radius b-b b-light pull-in')
						)); ?><!-- breadcrumbs -->
						<?php endif; ?>
						
						<?php echo $content; ?>
						<?php // echo $content; ?>
					</section>
						
					</div>
			   </section>
			   
			   
			   
			   
			   
               <aside class="bg-light lter b-l aside-md hide" id="notes">
                  <div class="wrapper">
                     Notification
                  </div>
               </aside>
            </section>
         </section>
      </section>

      <!-- Bootstrap -->

   </body>
</html>