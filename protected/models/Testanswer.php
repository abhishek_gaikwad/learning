<?php

/**
 * This is the model class for table "testanswer".
 *
 * The followings are the available columns in table 'testanswer':
 * @property integer $id
 * @property integer $testsession_id
 * @property integer $question_id
 * @property integer $multiplechoice_id
 *
 * The followings are the available model relations:
 * @property Question $question
 * @property Multiplechoice $multiplechoice
 * @property Testsession $testsession
 */
class Testanswer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'testanswer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('testsession_id, question_id', 'required'),
			array('testsession_id, question_id, multiplechoice_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, testsession_id, question_id, multiplechoice_id', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'question' => array(self::BELONGS_TO, 'Question', 'question_id'),
			'multiplechoice' => array(self::BELONGS_TO, 'Multiplechoice', 'multiplechoice_id'),
			'testsession' => array(self::BELONGS_TO, 'Testsession', 'testsession_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'testsession_id' => 'Testsession',
			'question_id' => 'Question',
			'multiplechoice_id' => 'Multiplechoice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('testsession_id',$this->testsession_id);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('multiplechoice_id',$this->multiplechoice_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Testanswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
