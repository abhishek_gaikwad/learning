<?php

class Test extends CActiveRecord
{
         protected $number = 5 ;
        public function tableName()
	{
		return 'test';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_id, topic_id, subtopic_id', 'numerical', 'integerOnly'=>true),
			array('test_name', 'length', 'max'=>45),
			array('created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, test_name, subject_id, topic_id, subtopic_id, created_at', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}
	public function relations()
	{
	return array(
			'subject' => array(self::BELONGS_TO, 'Subject', 'subject_id'),
			'topic' => array(self::BELONGS_TO, 'Topic', 'topic_id'),
			'subtopic' => array(self::BELONGS_TO, 'Subtopic', 'subtopic_id'),
			'testquestions' => array(self::HAS_MANY, 'Testquestion', 'test_id'),
			'testsessions' => array(self::HAS_MANY, 'Testsession', 'test_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'test_name' => 'Test Name',
			'subject_id' => 'Subject',
			'topic_id' => 'Topic',
			'subtopic_id' => 'Subtopic',
			'created_at' => 'Created At',
		);
	}
public function search()
	{
	$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('test_name',$this->test_name,true);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('topic_id',$this->topic_id);
		$criteria->compare('subtopic_id',$this->subtopic_id);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
   public static function createTest($theory) {
            $test = new Test ;
            $test->test_name = strtoupper(substr($theory->topic->subject->subject_name, 0, 3) . '-' . substr($theory->topic->topic_name, 0, 3) . '-' . rand(0, 99999)) ;
            $test->created_at = new CDbExpression("NOW()") ;
            
            $test->topic_id = $theory->topic_id ;
            $test->subject_id = $theory->subtopic_id ;
            $test->subject_id = $theory->topic->subject_id ;           
            $test->save() ;
            
            $test->setQuestion(1) ;
            
            return $test;
        }
protected function setQuestion($limit = 15) {
            $criteria = new CDbCriteria ;
            
            if($this->subtopic_id != null) {                
                $criteria->addInCondition('subtopic_id', array($this->subtopic_id));
            } else if($this->topic_id != null) {
                $criteria->addInCondition('topic_id', array($this->topic_id));
            } else {
                $criteria->addInCondition('subject_id', array($this->subject_id));
            }
            
            $criteria->limit = $limit ;
            $criteria->order = "RAND()" ;
            $questions = Question::model()->findAll($criteria) ;
            
            foreach ($questions as $question) {
                $temp = new Testquestion ;
                $temp->test_id = $this->id ;
                $temp->question_id = $question->id ;
                $temp->save() ;                       
            }  
        }        
        
}
