<?php

/**
 * This is the model class for table "word_tracking".
 *
 * The followings are the available columns in table 'word_tracking':
 * @property integer $id
 * @property integer $user_id
 * @property integer $word_id
 * @property integer $count
 * @property string $last_seen
 *
 * The followings are the available model relations:
 * @property Userdetails $user
 * @property Word $word
 */
class WordTracking extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'word_tracking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                    array('word_id, user_id', 'required'),
			array('user_id, word_id, count', 'numerical', 'integerOnly'=>true),
			array('last_seen', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, word_id, count, last_seen', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Userdetails', 'user_id'),
			'word' => array(self::BELONGS_TO, 'Word', 'word_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'word_id' => 'Word',
			'count' => 'Count',
			'last_seen' => 'Last Seen',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('word_id',$this->word_id);
		$criteria->compare('count',$this->count);
		$criteria->compare('last_seen',$this->last_seen,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WordTracking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
