<?php

/**
 * This is the model class for table "testsession".
 *
 * The followings are the available columns in table 'testsession':
 * @property integer $id
 * @property integer $user_id
 * @property integer $test_id
 * @property string $test_started_on
 * @property integer $test_completed
 * @property integer $marks
 * @property string $test_completed_on
 *
 * The followings are the available model relations:
 * @property Testanswer[] $testanswers
 * @property Test $test
 * @property Userdetails $user
 */
class Testsession extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'testsession';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, test_id, test_started_on', 'required'),
			array('user_id, test_id, test_completed, marks', 'numerical', 'integerOnly'=>true),
			array('test_completed_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, test_id, test_started_on, test_completed, marks, test_completed_on', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'testanswers' => array(self::HAS_MANY, 'Testanswer', 'testsession_id'),
			'test' => array(self::BELONGS_TO, 'Test', 'test_id'),
			'user' => array(self::BELONGS_TO, 'Userdetails', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'test_id' => 'Test',
			'test_started_on' => 'Test Started On',
			'test_completed' => 'Test Completed',
			'marks' => 'Marks',
			'test_completed_on' => 'Test Completed On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('test_id',$this->test_id);
		$criteria->compare('test_started_on',$this->test_started_on,true);
		$criteria->compare('test_completed',$this->test_completed);
		$criteria->compare('marks',$this->marks);
		$criteria->compare('test_completed_on',$this->test_completed_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Testsession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function evalute($answers) {
            $marks = 0 ;
            
            if($this->test_completed == 1)
                return true ;
            
            foreach($answers as $answer) {
                $t = new Testanswer ;
                
                $t->testsession_id = $this->id ;
                $t->question_id = $answer[0] ; 
                $t->multiplechoice_id = $answer[1] ; 
                
                $t->save() ;
                
               $question = Question::model()->findByPk($answer[0]) ;
               
               if($question->isRight($answer[1]))
                   $marks++ ;
                    
            }
            
            $this->marks = $marks ;
            $this->test_completed = 1 ;
            $this->test_completed_on = new CDbExpression("NOW()") ;
            return $this->save() ;
            
        }
        
        public static function takeTest($userID, $theoryID) {
            
            $theory = Theory::model()->findByPk($theoryID) ;
            
            $criteria = new CDbCriteria ;
            
            $criteria->with = 'test' ;
            
            $criteria->addInCondition('user_id', array($userID)) ;
            $criteria->addInCondition('test.topic_id', array($theory->topic_id)) ;
            $criteria->addInCondition('test.subtopic_id', array($theory->subtopic_id)) ;
            
            $session = Testsession::model()->findAll($criteria);
            
            
            $criteria2 = new CDbCriteria ;
            $criteria2->addInCondition('topic_id', array($theory->topic_id)) ;
            $criteria2->addInCondition('subtopic_id', array($theory->subtopic_id)) ;
            $tests = Test::model()->findAll($criteria2);
            
            if(sizeof($session) == 0) {
                //allocate any test for that theory
                if(sizeof($tests) == 0) {
                    $test = Test::createTest($theory) ;
                    
                } else {
                    //random from test
                    $test = $tests[array_rand($tests)] ;                    
                }
                
            } else {
                $sessionArray = array() ;
                $testArray = array() ;
                
                foreach($session as $i) {
                    array_push($sessionArray, $i->test_id) ;
                }
                
                foreach($tests as $i) {
                    array_push($testArray, $i->id) ;
                }                
                
                $diff = array_diff($testArray, $sessionArray);
                
                if(sizeof($diff) == 0) {
                    $test = Test::createTest($theory) ;
                } else {
                    $i = $diff[array_rand($diff)] ; //id of test
                    $test = Test::model()->findByPk($i) ;             
                }                
            }
            
            $s = new Testsession ;
            $s->user_id = $userID ;
            $s->test_id = $test->id ;
            $s->test_started_on = new CDbExpression("NOW()") ;
            $s->save() ;
            
            return $s ;            
            
        }
}
