<?php

/**
 * This is the model class for table "word".
 *
 * The followings are the available columns in table 'word':
 * @property integer $id
 * @property string $word_type
 * @property string $word_name
 * @property string $word_meaning
 *
 * The followings are the available model relations:
 * @property Sentence[] $sentences
 * @property Usersentence[] $usersentences
 * @property WordTracking[] $wordTrackings
 */
class Word extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
        public $sentence ;
        
	public function tableName()
	{
		return 'word';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('word_type, word_name, word_meaning, sentence', 'required'),
			array('word_type, word_name', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, word_type, word_name, word_meaning, sentence', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sentences' => array(self::HAS_MANY, 'Sentence', 'word_id'),
			'usersentences' => array(self::HAS_MANY, 'Usersentence', 'word_id'),
			'wordTrackings' => array(self::HAS_MANY, 'WordTracking', 'word_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'word_type' => 'Word Type',
			'word_name' => 'Word Name',
			'word_meaning' => 'Word Meaning',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('word_type',$this->word_type,true);
		$criteria->compare('word_name',$this->word_name,true);
		$criteria->compare('word_meaning',$this->word_meaning,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Word the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function saveSentence() {            
            $model = new Sentence ;
            $model->sentence = $this->sentence ;
            $model->word_id = (int)$this->id ;
            
            return $model->save() ;
            //var_dump($model->errors);
        }
        
        public function updateSentence() {            
            $model = $this->sentences[0] ;
            $model->sentence = $this->sentence ;
            $model->word_id = (int)$this->id ;
            
            return $model->update() ;
            //var_dump($model->errors);
        }
        
        public function saveAll() {
            if($this->save())
                return $this->saveSentence () ;            
        }
        
        public function update($attributes = null) {
            parent::update($attributes);
            return $this->updateSentence();
        }
        
        protected function afterFind() {
            parent::afterFind();
            $this->sentence = $this->sentences[0]->sentence ;
        }
        
        public function addSentence($userID, $sentence) {
            $model = new Usersentence ;
            $model->added_on = new CDbExpression("NOW()");
            $model->word_id = (int)$this->id ;
            $model->user_id = (int)$userID ;
            $model->sentence = $sentence ;
            
            return $model->save() ;
        }
        
        public function editSentence($sentenceID, $sentence) {
            $model = Usersentence::model()->findByPk($sentenceID) ;
            $model->sentence = $sentence ;
            $model->added_on = new CDbExpression("NOW()");
            
            return $model->save() ;
        }
        
        public function updateWordTracking($userID) {
            
            $model = WordTracking::model()->findByAttributes(array("user_id" => $userID, "word_id" => $this->id)) ;
            
            if($model == null)
                $model = new WordTracking ;
            
            $model->user_id = $userID ;
            $model->word_id = $this->id ;
            
            
            $model->last_seen = new CDbExpression("NOW()") ;
            $model->count = (int)$model->count + 1 ;
            
            return $model->save() ;
            
        }
        
        public function userSentences($userID) {
            return Usersentence::model()->findAllByAttributes(array('user_id'=>$userID, 'word_id' => $this->id)) ;
        }
            
        
        public function getNextId()
        {
            $record=self::model()->find(array(
                    'condition' => 'id>:current_id',
                    'order' => 'word_name ASC',
                    'limit' => 1,
                    'params'=>array(':current_id'=>$this->id),
            ));
            if($record!==null)
                return $record->id;
            return null;
        }
        public function getPreviousId()
        {
            $record=self::model()->find(array(
                    'condition' => 'id<:current_id',
                    'order' => 'word_name DESC',
                    'limit' => 1,
                    'params'=>array(':current_id'=>$this->id),
            ));
            if($record!==null)
                return $record->id;
            return null;
        }
        
}
