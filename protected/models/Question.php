<?php

/**
 * This is the model class for table "question".
 *
 * The followings are the available columns in table 'question':
 * @property integer $id
 * @property integer $topic_id
 * @property integer $subtopic_id
 * @property string $question_type
 * @property string $question_description
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property Multiplechoice[] $multiplechoices
 * @property Subtopic $subtopic
 * @property Topic $topic
 * @property Solution[] $solutions
 * @property Testquestion[] $testquestions
 */
class Question extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('topic_id, created_at', 'required'),
			array('topic_id, subtopic_id', 'numerical', 'integerOnly'=>true),
			array('question_type', 'length', 'max'=>45),
                        array('question_description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, topic_id, subtopic_id, question_type, question_description, created_at', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'multiplechoices' => array(self::HAS_MANY, 'Multiplechoice', 'question_id'),
			'subtopic' => array(self::BELONGS_TO, 'Subtopic', 'subtopic_id'),
			'topic' => array(self::BELONGS_TO, 'Topic', 'topic_id'),
			'solutions' => array(self::HAS_MANY, 'Solution', 'question_id'),
			'testquestions' => array(self::HAS_MANY, 'Testquestion', 'question_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'topic_id' => 'Topic',
			'subtopic_id' => 'Subtopic',
			'question_type' => 'Question Type',
			'question_description' => 'Question Description',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('topic_id',$this->topic_id);
		$criteria->compare('subtopic_id',$this->subtopic_id);
		$criteria->compare('question_type',$this->question_type,true);
		$criteria->compare('question_description',$this->question_description,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Question the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function isRight($mid) {
            $answer = Solution::model()->findByPk($this->id) ;
            
            if($answer->multiplechoice_id == $mid)
                return true ;
            else 
                return false ;            
        }
        
        public function answer() {
            return $this->solutions[0] ;            
        }
}
