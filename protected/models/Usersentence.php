<?php

/**
 * This is the model class for table "usersentence".
 *
 * The followings are the available columns in table 'usersentence':
 * @property integer $id
 * @property integer $user_id
 * @property integer $word_id
 * @property string $sentence
 * @property string $added_on
 *
 * The followings are the available model relations:
 * @property Userdetails $user
 * @property Word $word
 */
class Usersentence extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usersentence';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, word_id, sentence, added_on', 'required'),
			array('user_id, word_id', 'numerical', 'integerOnly'=>true),
			array('sentence', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, word_id, sentence, added_on', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Userdetails', 'user_id'),
			'word' => array(self::BELONGS_TO, 'Word', 'word_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'word_id' => 'Word',
			'sentence' => 'Sentence',
			'added_on' => 'Added On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('word_id',$this->word_id);
		$criteria->compare('sentence',$this->sentence,true);
		$criteria->compare('added_on',$this->added_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usersentence the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
