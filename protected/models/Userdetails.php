<?php

/**
 * This is the model class for table "userdetails".
 *
 * The followings are the available columns in table 'userdetails':
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $email_id
 * @property string $photo_name
 * @property string $photo_location
 * @property string $created_at
 * @property integer $active_domain
 * @property integer $uid
 *
 * The followings are the available model relations:
 * @property Friendship[] $friendships
 * @property Friendship[] $friendships1
 * @property Testsession[] $testsessions
 * @property Domain $activeDomain
 * @property Users $u
 * @property Usersentence[] $usersentences
 * @property WordTracking[] $wordTrackings
 */
class Userdetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userdetails';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, email_id, created_at', 'required'),
			array('active_domain, uid', 'numerical', 'integerOnly'=>true),
			array('first_name, middle_name, last_name, photo_location', 'length', 'max'=>45),
			array('email_id', 'length', 'max'=>100),
			array('photo_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, middle_name, last_name, email_id, photo_name, photo_location, created_at,active_domain, uid', 'safe', 'on'=>'search'),
			array('email_id', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Email Address Already Added'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'friendships' => array(self::HAS_MANY, 'Friendship', 'friend_id'),
			'friendships1' => array(self::HAS_MANY, 'Friendship', 'user_id'),
			'testsessions' => array(self::HAS_MANY, 'Testsession', 'user_id'),
                        'activeDomain' => array(self::BELONGS_TO, 'Domain', 'active_domain'),
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
			'usersentences' => array(self::HAS_MANY, 'Usersentence', 'user_id'),
			'wordTrackings' => array(self::HAS_MANY, 'WordTracking', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'email_id' => 'Email',
			'photo_name' => 'Photo Name',
			'photo_location' => 'Photo Location',
			'created_at' => 'Created At',
			'uid' => 'Uid',
                        'active_domain' => 'Active Domain',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email_id',$this->email_id,true);
		$criteria->compare('photo_name',$this->photo_name,true);
		$criteria->compare('photo_location',$this->photo_location,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('uid',$this->uid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Userdetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
              
        public function addSentence($wordID, $sentence) {
            $model = new Usersentence ;
            $model->added_on = new CDbExpression("NOW()");
            $model->word_id = $wordID ;
            $model->user_id = $this->id ;
            $model->sentence = $sentence ;
            
            return $model->save() ;
        }
        
        public function editSentence($sentenceID, $sentence) {
            $model = Usersentence::model()->findByPk($sentenceID) ;
            $model->sentence = $sentence ;
            $model->added_on = new CDbExpression("NOW()");
            
            return $model->save() ;
        }
        
        public function userSentences() {
            return Usersentence::model()->findAllByAttributes(array('user_id' => $this->id)) ;
        }
        
        public function updateWord($wordID) {
            
            $model = WordTracking::model()->findByAttributes(array("user_id" => $this->id, "word_id" => $wordID)) ;
            
            if($model == null)
                $model = new WordTracking ;
            
            $model->user_id = $this->id ;
            $model->word_id = $wordID ;
            
            $model->last_seen = new CDbExpression("NOW()") ;
            $model->count = (int)$model->count + 1 ;
            
            return $model->save() ;
            
        }
        
        public function newTest($topicID, $subTopicID = null) {
            $question ;
         
        }
        
        public function setDomain($domainID) {            
            $this->active_domain = $domainID ;
            return $this->save() ;
        }
        
        public function pendingTest() {
           $test = Testsession::model()->findAllByAttributes(array('user_id' => $this->id, 'test_completed' => 0)) ;
           
           return $test ;
        }
        
        public function completedTest() {
           $test = Testsession::model()->findAllByAttributes(array('user_id' => $this->id, 'test_completed' => 1)) ;
           
           return $test ;
        }
        
}
