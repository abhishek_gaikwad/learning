<?php

/**
 * This is the model class for table "subtopic".
 *
 * The followings are the available columns in table 'subtopic':
 * @property integer $id
 * @property string $subtopic_name
 * @property integer $topic_id
 *
 * The followings are the available model relations:
 * @property Question[] $questions
 * @property Topic $topic
 * @property Test[] $tests
 * @property Theory[] $theories
 */
class Subtopic extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        public $subject_search ;
         
	public function tableName()
	{
		return 'subtopic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subtopic_name, topic_id', 'required'),
			array('topic_id', 'numerical', 'integerOnly'=>true),
			array('subtopic_name', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, subtopic_name, topic_id, subject_search', 'safe', 'on'=>'search'),
			//array('col1, col2', 'ECompositeUniqueValidator','attributesToAddError'=>'tag','message'=>'Error Message'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'questions' => array(self::HAS_MANY, 'Question', 'subtopic_id'),
			'topic' => array(self::BELONGS_TO, 'Topic', 'topic_id'),
			'theories' => array(self::HAS_MANY, 'Theory', 'subtopic_id'),
                        'tests' => array(self::HAS_MANY, 'Test', 'subtopic_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subtopic_name' => 'Subtopic Name',
			'topic_id' => 'Topic',
                        'subject_search' => 'Subject'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('subtopic_name',$this->subtopic_name,true);
		$criteria->compare('topic_id',$this->topic_id);
		
                $criteria->compare('topic.subject_id',$this->subject_search);
                
                $criteria->with = array('topic' => array(
                   //'with' => 'subject',
                )) ;
                        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subtopic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
