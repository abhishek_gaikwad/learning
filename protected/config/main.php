<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Social E-Learning',
    //'defaultController' => 'user/login',
    'timeZone' => 'Asia/Calcutta' ,
    'preload' => array(
        'log'
    ),
    'defaultController' => 'user/login', 
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.ECompositeUniqueValidator',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*'
        
    ),
    
    'modules' => array(
        // uncomment the following to enable the Gii tool
        
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '1',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array(
                '127.0.0.1',
                '::1'
            )
        ),
        
        'user' => array(
            'tableUsers' => 'users',
            'tableProfiles' => 'profiles',
            'tableProfileFields' => 'profiles_fields',
            
            # encrypting method (php hash function)
            'hash' => 'md5',
            
            # send activation email
            'sendActivationMail' => true,
            
            # allow access for non-activated users
            'loginNotActiv' => false,
            
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,
            
            # automatically login from registration
            'autoLogin' => true,
            
            # registration path
            'registrationUrl' => array(
                '/user/registration'
            ),
            
            # recovery password path
            'recoveryUrl' => array(
                '/user/recovery'
            ),
            
            # login form path
            'loginUrl' => array(
                '/user/login'
            ),
            
            # page after login
            //'returnUrl' => array('/user/profile'),
            
            # page after logout
            'returnLogoutUrl' => array(
                '/user/login'
            )
        ),
        
        'rights' => array(
            'superuserName' => 'Admin', // Name of the role with super user privileges. 
            'authenticatedName' => 'Authenticated', // Name of the authenticated user role. 
            'userIdColumn' => 'id', // Name of the user id column in the database. 
            'userNameColumn' => 'username', // Name of the user name column in the database. 
            'enableBizRule' => true, // Whether to enable authorization item business rules. 
            'enableBizRuleData' => true, // Whether to enable data for business rules. 
            'displayDescription' => true, // Whether to use item description instead of name. 
            'flashSuccessKey' => 'RightsSuccess', // Key to use for setting success flash messages. 
            'flashErrorKey' => 'RightsError', // Key to use for setting error flash messages. 
            //'install'=>true,  // Whether to install rights. 
            'baseUrl' => '/rights', // Base URL for Rights. Change if module is nested. 
            'layout' => 'rights.views.layouts.main', // Layout to use for displaying Rights. 
            'appLayout' => 'application.views.layouts.main', // Application layout. 
            'cssFile' => 'rights.css', // Style sheet file to use for Rights. 
            'install' => false, // Whether to enable installer. 
            'debug' => false
        ),
        'account',
        'domain', 
        'subject', 
        'topic', 
        'theory',
        'question',
        'word',
        'test',
        
        
    ),
    
    // application components
    'components' => array(
        'authManager' => array(
            'class' => 'RDbAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array(
                'Authenticated',
                'Guest'
            )
        ),
        'clientScript' => array(
            'packages' => array(
                'jquery'=>array(
			  //'baseUrl'=>'//ajax.googleapis.com/ajax/libs/jquery/1.8/',
                            'baseUrl'=>'js/',
			  'js'=>array('jquery-1.11.0.js'),
			  'coreScriptPosition'=>CClientScript::POS_HEAD
			),
			'jquery.ui'=>array(
			    'baseUrl'=>'js/',                            
                            'js'=>array('jquery-ui.min.js'),
                            'css'=>array('jquery-ui.min.css'),
			  'depends'=>array('jquery'),
			  'coreScriptPosition'=>CClientScript::POS_BEGIN
			  
			),
			'jquery.ui.core'=>array(
                            'baseUrl'=>'js/',                            
                            'js'=>array('jquery.ui.core.js'),
                            'depends'=>array('jquery'),
                            'coreScriptPosition'=>CClientScript::POS_BEGIN
			  
			),
			'jquery.ui.autocomplete'=>array(
                            'baseUrl'=>'js/autocomplete/',                            
                            'js'=>array('jquery.autocomplete.js'),
                            'css'=>array('jquery.autocomplete.css'),
                            'depends'=>array('jquery'),
                            //'coreScriptPosition'=>CClientScript::POS_BEGIN
			  
			),
                'app' => array(
                    'baseUrl' => 'js/',
                    'js' => array(
                        'app.v2.js'
                    ),
                    'depends' => array(
                        'jquery'
                    ),
                    'coreScriptPosition' => CClientScript::POS_END
                ),
                'combodate' => array(
                    'baseUrl' => 'js/combodate/',
                    'depends' => array(
                        'moment'
                    ),
                    'js' => array(
                        'combodate.js'
                    )
                    //'coreScriptPosition'=>CClientScript::POS_END
                ),
                'moment' => array(
                    'baseUrl' => 'js/moment/',
                    'depends' => array(
                        'app'
                    ),
                    'js' => array(
                        'moment.min.js'
                    )
                    //'coreScriptPosition'=>CClientScript::POS_END
                ),
                
                'fuelux' => array(
                    'baseUrl' => 'js/fuelux/',
                    'depends' => array(
                        'app'
                    ),
                    'js' => array(
                        'fuelux.js'
                    ),
                    'css' => array(
                        'fuelux.css'
                    )
                    //'coreScriptPosition'=>CClientScript::POS_END
                ),
                'autocomplete' => array(
                    'baseUrl' => 'js/autocomplete/',
                    'depends' => array(
                        'app'
                    ),
                    'js' => array(
                        'jquery.autocomplete.js'
                    ),
                    'css' => array(
                        'jquery.autocomplete.css'
                    )
                    //'coreScriptPosition'=>CClientScript::POS_END
                ),
                'nestable' => array(
                    'baseUrl' => 'js/nestable/',
                    'depends' => array(
                        'app'
                    ),
                    'js' => array(
                        'jquery.nestable.js'
                    ),
                    'css' => array(
                        'nestable.css'
                    )
                    //'coreScriptPosition'=>CClientScript::POS_END
                )
            )
        ),
        
        'user' => array(
            // enable cookie-based authentication
            'class' => 'RWebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array(
                '/user/login'
            )
        ),
        // uncomment the following to enable URLs in path-format
        /*
        'urlManager'=>array(
        'urlFormat'=>'path',
        'rules'=>array(
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
        ),
        ),
        
        'db'=>array(
        'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
        ),
        // uncomment the following to use a MySQL database
        */
        'db' => (defined('DB_CONNECTION') ? array(
            'connectionString' => DB_CONNECTION,
            'username' => DB_USER,
            'password' => DB_PWD,
            'charset' => 'utf8',
            'emulatePrepare' => true,
            'enableParamLogging' => true,
            'initSQLs' => array(
                "set time_zone='+00:00';"
            )
        ) : array()),
        
        
        
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error'
        ),
'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'trace,log',
					'categories' => 'system.db.CDbCommand',
					'logFile' => 'db.log',
				 ), 
//                                array(
//					'class'=>'CFileLogRoute',
//					'levels'=>'',
//					'categories' => 'system.db.CDbTransaction',
//					'logFile' => 'db2.log',
//				 ), 
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
    ),
    
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com'
    )
);